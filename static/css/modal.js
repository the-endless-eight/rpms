// Get the modal
var signupModal = document.getElementById("patientModal");

// Get the button that opens the modal
var signupBtn = document.getElementById("patientBtn");

// Get the button element that closes the modal
var signupButton = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
signupBtn.onclick = function () {
  patientModal.style.display = "block";
};

// When the user clicks on button(cancel), close the modal
signupButton.onclick = function () {
  patientModal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
  if (event.target == patientModal) {
    patientModal.style.display = "none";
  }
};
