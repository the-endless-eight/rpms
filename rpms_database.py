from flask import Flask, flash
import random
from hashlib import md5
from hospitalOwner import hospitalOwner
from communityOwner import communityOwner
from doctor import doctor
from patient import patient


# Returns true if the hashed password argument matches the hashed inputted plaintext password.
def compare_password_hash(hashed_password, password):
    print(hashed_password)
    print(password + "->" + hash_password(password))
    return hashed_password == hash_password(password)


# Password argument is hashed into md5 and returned.
# Might change from MD5 to something more secure, but deciding on which algorithm still,
# Not sure if it's really necessary for this.
def hash_password(password):
    return md5(password.encode("utf-8")).hexdigest()


# Generates an integer of length 12 (skips 0 to 99999999999 for simplicity in code)
def generate_random_id():
    return random.randint(100000000000, 999999999999)


class Database:
    def __init__(self, dbfile):
        self.db = dbfile

    def add_user(self, form):

        if "family-id" in form.keys():
            # QUERY TO SEE IF FAMILY ID EXISTS - if it doesn't fail signup
            pass
        if "community-id" in form.keys():
            # QUERY TO SEE IF FAMILY ID EXISTS - if it doesn't fail signup
            pass
        if "hospital-id" in form.keys():
            # QUERY TO SEE IF FAMILY ID EXISTS - if it doesn't fail signup
            pass

        if (
                form["password"] != form["confirm-password"]
        ):  # Passwords don't match to confirm sign-up.
            flash("ERROR: Passwords did not match.")
            return

        uid = generate_random_id()
        self.db.user.insert(
            {
                "UserID": uid,
                "FirstName": form["fname"],
                "LastName": form["lname"],
                "Address": form["address-1"],
                "Address-2": form["address-2"],
                "Birthday": form["birth-date"],
                "IsAdmin": False,
            }
        )

        self.db.credentials.insert(
            {
                "UserID": uid,
                "SSN": hash_password(form["ssn"]),
                "Email": form["email"],
                "Password": hash_password(form["password"]),
            }
        )

        # THIS WILL RUN ALL FUNCTIONS IF THESE ARE KEYS ALL PRESENT. WE NEED TO DECIDE ON HOW WE'RE DOING REGISTRATIONS.
        if "typebox" in form.keys():
            if form["typebox"] == "patient":
                pat = patient(uid, form["community-id"])
                pat.add_patient(form)
                # self.add_patient(uid, form)
                flash("Patient created successfully")
            elif form["typebox"] == "doctor":
                doc = doctor(uid, form["community-id"], form["hospital-id"])
                doc.add_doctor()
                flash("Doctor created successfully")
            elif form["typebox"] == "community":
                co = communityOwner(
                    form["community-name"], form["community-address"], uid
                )
                co.add_community_owner()
                flash("Community owner created successfully")
            elif form["typebox"] == "hospital":
                ho = hospitalOwner(
                    uid,
                    form["hospital-name"],
                    form["community-id"],
                    form["hospital-address"],
                )
                ho.add_hospital_owner()
                flash("Hospital owner created successfully")
        else:
            flash("Empty user created successfully")

    def get_user_email(self, uid):
        return self.db.credentials.find_one({"UserID": uid}, {"Email": 1})

    # Returns a dict variable of UserID, FirstName, LastName if a user is found.
    def get_name_from_userid(self, uid):
        v = list(
            self.db.user.find(
                {"UserID": uid}, {"_id": 0, "UserID": 1, "FirstName": 1, "LastName": 1}
            )
        )
        if len(v) > 0:
            return v[0]
        else:
            return None

    # Returns all user objects in the user collection.
    def get_users(self):
        return self.db.user.find({})

    # Returns all data from the user collection based off a given UserID
    def get_user(self, uid):
        return self.db.user.find_one({"UserID": uid})

    def populate_user_info(self, user):
        uid = user.userID
        user.patient = self.db.patient.find({"UserID": uid})
        user.doctor = self.db.doctor.find({"UserID": uid})
        user.community = self.db.community.find({"CommunityOwnerID": uid})
        user.hospital = self.db.community.find({"HospitalOwnerID": uid})
        return user

    def get_credentials_by_email(self, email):
        return self.db.credentials.find_one({"Email": email})

    # Check if user is admin
    def get_isAdmin(self, UserID):
        admin = self.db.user.find_one({"UserID": UserID})
        is_admin = admin["IsAdmin"]
        return is_admin

    # Set user as admin
    def set_isAdmin_true(self, UserID):
        self.db.user.update_one({"UserID": UserID}, {"$set": {"IsAdmin": True}})

    def list_patient(self, CommunityID):
        userid_list = list(
            self.db.patient.find(
                {"CommunityApproved": True, "CommunityID": CommunityID},
                {"UserID": 1, "FirstName": 1, "LastName": 1},
            ).sort("UserID", 1)
        )
        return userid_list

    # Implemented these to fix approvals page, could not grab from database inside Classes
    def list_doctor(self):
        userid_list = list(
            self.db.doctor.find(
                {"$or": [{"CommunityApproved": False}, {"HospitalApproved": False}]},
                {"UserID": 1, "FirstName": 1, "LastName": 1},
            )
        )
        return userid_list

    # Implemented these to fix approvals page, could not grab from database inside Classes
    def list_communities(self):
        communities = list(
            self.db.community.find(
                {"Approved": False},
                {
                    "_id": 0,
                    "CommunityID": 1,
                    "CommunityName": 1,
                    "Address": 1,
                    "CommunityOwnerID": 1,
                },
            )
        )
        return communities

    # Implemented these to fix approvals page, could not grab from database inside Classes
    def list_hospital(self):
        hospitals = list(
            self.db.hospital.find(
                {"Approved": False},
                {"_id": 0, "HospitalID": 1, "HospitalName": 1, "HospitalOwnerID": 1},
            )
        )
        return hospitals

    def get_biometric_tests(self):
        bio_tests = list(
            self.db.biometric_test.find({})
        )
        return bio_tests

    def get_patient_visits(self, doctorid, patient):
        visits = list(
            self.db.doctor_patient_visit.find(
                {'DoctorID': doctorid, 'Patient': patient}
            )
        )
        return visits

    def get_visit(self, visitid):
        visit = list(
            self.db.doctor_patient_visit.find(
                {'VisitID': visitid}
            )
        )
        if len(visit) > 0:
            return visit[0]
        else:
            return None

    def get_prescription(self, visit):
        prescription = list(
            self.db.prescription.find(
                {'VisitID': visit}
            )
        )
        return prescription

    def get_matching_patients(self, doctorid):
        patients_unduped = list(
            self.db.doctor_patient_visit.find(
                {'DoctorID': doctorid}
            )
        )
        patients = set()
        for patient in patients_unduped:
            patients.add(patient['Patient'])
        return list(patients)

    def get_hospitals_and_communities(self,communityid=None):
        query_constraints = {}
        if communityid != None:
            query_constraints |= {'CommunityID': str(communityid)}
        hospitals_full = list(self.db.hospital.find(query_constraints))
        community_full = list(self.db.community.find(query_constraints))
        hospitals = []
        for hospital in hospitals_full:
            hospital = {'HospitalID': hospital['HospitalID'], 'HospitalName': hospital['HospitalName']}
            hospitals.append(hospital)
        communities = []
        for community in community_full:
            community = {'CommunityID': community['CommunityID'], 'CommunityName': community['CommunityName']}
            communities.append(community)
        return hospitals, communities

    def get_community_from_ownerid(self,ownerid):
        return self.db.community.find({"CommunityOwnerID":ownerid})[0]