from flask import current_app, flash
import random
from biometricTest import biometricTest

# Generates an integer of length 12 (skips 0 to 99999999999 for simplicity in code)
def generate_random_id():
    return random.randint(100000000000, 999999999999)

class record:
    def __init__(self, *args):
        self.db = current_app.config["mdb"].db
        if len(args) > 1:
            self.PatientID = args[0]
            self.TestID = args[1]
            self.TestValue = args[2]
            self.DateSubmitted = args[3]
        elif len(args) == 2:
            self.PatientID = args[0]
            self.TestID = args[1]

    # Add record of a patient test
    def add_patient_record(self):
        self.db.record.insert(
            {
                "RecordID": generate_random_id(),
                "PatientID": self.PatientID,
                "TestID": self.TestID,
                "TestValue": self.TestValue,
                "DateSubmitted": self.DateSubmitted,
            }
        )
        bt = biometricTest(self.TestID)
        test = bt.get_test_from_id()
        lower_limit = test["LowerLimit"]
        upper_limit = test["UpperLimit"]
        if (float(self.TestValue) < lower_limit):
            flash(test["TestName"] + " is too low, consult your doctor")
        if (float(self.TestValue) > upper_limit):
            flash(test["TestName"] + " is too high, consult your doctor")

    # Grab a list of date sorted test values based on patient id and test id
    def get_sorted_patient_record_by_test(self):
        patient_record = list(
            self.db.record.find(
                {"PatientID": self.PatientID, "TestID": self.TestID}, {"TestValue": 1, "DateSubmitted": 1}
            ).sort('DateSubmitted')
        )
        return patient_record

    # Grab a 2-D list of a patient's submitted dates and their corresponding test values
    def get_records_by_date(self):
        records_dates = self.db.record.distinct("DateSubmitted", {"PatientID": self.PatientID})
        records_dates.sort()
        records = []
        for date in records_dates:
            record = []
            record.append(date)
            test_values = self.db.record.find({"DateSubmitted": date, "PatientID": self.PatientID})
            for test_value in test_values:
                record.append(test_value["TestValue"])
            records.append(record)
        return records