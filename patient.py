from flask import current_app, flash
import random

# Generates an integer of length 12 (skips 0 to 99999999999 for simplicity in code)
def generate_random_id():
    return random.randint(100000000000, 999999999999)

class patient:
    # constructor
    def __init__(self, *args):
        self.db = current_app.config["mdb"].db
        if len(args) > 1:
            self.UserID = args[0]
            self.CommunityID = args[1]
            self.CommunityApproved = False
            self.FirstOfFamily = None
            self.FamilyID = None
            self.PrevFamilyID = None
            self.FamilyInfo = ""
        elif len(args) == 1:
            self.UserID = args[0]
            self.FamilyID = args[0]
            self.CommunityID = args[0]

    def add_family(self, form):
        self.FamilyID = generate_random_id()
        # Immutable dict, so I can't directly set "family_info" to nothing
        if "family-info" in form.keys():
            self.FamilyInfo = form["family-info"]
        self.db.family.insert(
            {"FamilyID": self.FamilyID, "FamilyInfo": self.FamilyInfo}
        )
        return self.FamilyID

    def add_patient(self, form):
        if "family-id" in form.keys() and form["family-id"] is not None:
            self.FirstOfFamily = False
            self.FamilyID = form["family-id"]
        elif "firstoffamilybox" in form.keys():
            if form["firstoffamilybox"] == "first":
                self.FamilyID = self.add_family(form)
                self.FirstOfFamily = True

        if (
            self.FirstOfFamily is None or self.FamilyID is None
        ):  # Random case which shouldn't happen.
            self.FamilyID = self.add_family(form)
            self.FirstOfFamily = True

        self.db.patient.insert(
            {
                "UserID": self.UserID,
                "FirstOfFamily": self.FirstOfFamily,
                "FamilyID": self.FamilyID,
                "PrevFamilyID": None,  # assuming we're not taking input for this on account creation
                "CommunityID": self.CommunityID,
                "CommunityApproved": False,
            }
        )

    # Return all patients that don't have their community approved
    def get_unapproved_patients(self):
        userid_list = list(
            self.db.patient.find({"CommunityApproved": False}, {"_id": 0, "UserID": 1})
        )
        patient_information = []
        db = current_app.config["mdb"]
        for userid in userid_list:
            patient_information.append(db.get_name_from_userid(userid["UserID"]))
        while None in patient_information:
            patient_information.remove(None)
        return patient_information

    # Set patient approval to true. (Send in ID in form as UserID)
    def set_patient_approval(self):
        return self.db.patient.update(
            {"UserID": self.UserID}, {"$set": {"CommunityApproved": True}}
        )

    # Delete all main collection data for patient
    def delete_patient(self):
        patient = list(
            self.db.patient.find(
                {"UserID": self.UserID}, {"FirstOfFamily": 1, "FamilyID": 1}
            )
        )

        if len(patient) == 0:
            return
        patient = patient[0]

        # cleans up families or transfers
        if patient["FirstOfFamily"]:
            family_members = list(self.db.patient.find({"FamilyID": patient["FamilyID"]}))
            transferred = False
            for family_member in family_members:
                if self.UserID != family_member["UserID"]:
                    self.db.patient.update(
                        {"UserID": family_member["UserID"]}, {"FirstOfFamily": True}
                    )
                    transferred = True
            if not transferred:
                self.db.family.delete_one({"FamilyID": patient["FamilyID"]})

        self.db.patient.delete_one({"UserID": self.UserID})
        self.db.user.delete_one({"UserID": self.UserID})
        self.db.credentials.delete_one({"UserID": self.UserID})
        flash("Patient deleted successfully")

