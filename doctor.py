from flask import current_app, flash
import random

# Generates an integer of length 12 (skips 0 to 99999999999 for simplicity in code)
def generate_random_id():
    return random.randint(100000000000, 999999999999)

class doctor:

    # constructor
    def __init__(self, *args):
        self.db = current_app.config["mdb"].db
        if len(args) > 1:
            self.UserID = args[0]
            self.CommunityID = args[1]
            self.HospitalID = args[2]
            self.CommunityApproved = False
            self.HospitalApproved = False
        elif len(args) == 1:
            self.UserID = args[0]
            self.CommunityID = args[0]
            self.HospitalID = args[0]

    # MISSING CHECK IF HospitalID EXISTS IN DATABASE CURRENTLY IF IT'S GIVEN BY USER INPUT
    # MISSING CHECK IF CommunityID EXISTS IN DATABASE CURRENTLY IF IT'S GIVEN BY USER INPUT
    def add_doctor(self):
        # NEEDS TO CHECK IF COMMUNITY ID AND HOSPITAL ID ARE GIVEN. FORM SAYS REQUIRED, BUT ONLY
        self.db.doctor.insert(
            {
                "UserID": self.UserID,
                "CommunityID": self.CommunityID,  # IS THIS GIVEN? - currently says for name input, might need to
                # search name prior to assigning
                "CommunityApproved": False,
                "HospitalID": self.HospitalID,  # IS THIS GIVEN? - currently says for name input, might need to
                # search name prior to assigning - not needed if independent practitioner
                "HospitalApproved": False,  # Make this None if not needed if independent practitioner
            }
        )

    # DELETES ALL MAIN COLLECTION DATA FOR DOCTOR
    def delete_doctor(self):
        self.db.doctor.delete_one({"UserID": self.UserID})
        self.db.user.delete_one({"UserID": self.UserID})
        self.db.credentials.delete_one({"UserID": self.UserID})

    # Returns all doctors that don't meet both approval criteria.
    def get_unapproved_doctors(self):
        db = current_app.config["mdb"]
        userid_list = db.list_doctor()
        doctor_information = []
        for userid in userid_list:
            doctor_information.append(db.get_name_from_userid(userid["UserID"]))
        while None in doctor_information:
            doctor_information.remove(None)
        return doctor_information

    # Doesn't take into account who is approving. Community owner and hospital owner are usually different.
    def set_doctor_approval(self, uid):
        return self.db.doctor.update(
            {"UserID": uid},
            {"$set": {"CommunityApproved": True, "HospitalApproved": True}},
        )

    # Grabs a list of patients in the doctor's community
    def get_doctors_patients(self):
        db = current_app.config["mdb"]
        patient_list = []
        patients_user_ids = db.list_patient(self.CommunityID)
        for user_id in patients_user_ids:
            patient_list.append(db.get_name_from_userid(user_id["UserID"]))
        while None in patient_list:
            patient_list.remove(None)
        return patient_list

    def submitVisitForm(self, userID, form):
        visitID = generate_random_id()
        # if the prescription checkbox is checked, submit a prescription

        if form.get("has_prescription") is not None:
            submitPrescription(form, visitID)
        # if the lab test checkbox is checked, submit a lab test
        if form.get("has_lab") is not None:
            submitLabTest(form, visitID)
        patient = current_app.config['mdb'].db.credentials.find_one({
            "Email": form["patient_email"]
        })
        current_app.config['mdb'].db.doctor_patient_visit.insert({
            "VisitID": generate_random_id(),
            "DoctorID": userID,
            "Patient": patient['UserID'],
            "SymptomNotes": form["Symptoms_notes"],
            "MeetingNotes": form["Meeting_notes"]
        })
        return None


# private helper method for submitting a visit form
def submitPrescription(form, visitID):
    current_app.config['mdb'].db.prescription.insert({
        "PrescriptionID": generate_random_id(),
        "VisitID": visitID,
        "DiagnosisCode": form["prescription_diagnosis_code"],
        "Drug": form["script_drug"],
        "Dosage": form["script_drug"],
        "Timing": form["script_timing"],
        "Amount": form["script_amount"],
        "DEANumber": form["prescription_DEA_number"],
        "MedicalLicenseNumber": form["prescription_licence_number"],
        "SpecialInstructions": form["prescription_instructions"]
    })
    return None


# private helper method for submitting a visit form
def submitLabTest(form, visitID):
    current_app.config['mdb'].db.lab_test.insert({
        "TestID": generate_random_id(),
        "HospitalID": form["lab_test_location"],
        "VisitID": visitID,
        "TestName": form["lab_test_name"],
        "Warrant": form["lab_test_warrant"],
        "Instructions": form["lab_test_instructions"]
    })
    return None

