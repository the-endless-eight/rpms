import io
import random
from functools import wraps

from flask import Flask, current_app, render_template, request, redirect, url_for, flash, make_response, Response

import admin
from user import get_user_object
from flask_login import login_required, login_user, logout_user, current_user
from rpms_database import compare_password_hash
from hospitalOwner import hospitalOwner
from communityOwner import communityOwner
from doctor import doctor
from biometricTest import biometricTest
from record import record
from datetime import datetime
from patient import patient
from appointment import appointment
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg

app = Flask(__name__)


def requiresUserType(user=""):
    def decorator(function):
        @wraps(function)
        @login_required
        def wrapper(*args, **kwargs):
            if not current_app.config['enforces_user_type']:
                return function(*args, **kwargs)
            if user == "patient" and not current_user.is_patient:
                return "<h2><b>Error: url you entered is only available to patients</b></h2>"
            if user == "doctor" and not current_user.is_doctor:
                return "<h2><b>Error: url you entered is only available to doctors</b></h2>"
            if user == "community_owner" and not current_user.is_community_owner:
                return "<h2><b>Error: url you entered is only available to community owners</b></h2>"
            if user == "hospital_owner" and not current_user.is_hospital_owner:
                return "<h2><b>Error: url you entered is only available to hospital owners</b></h2>"
            if user == "admin" and not current_user.is_admin:
                print("")
             #   return "<h2><b>Error: url you entered is only available to admin</b></h2>"
            return function(*args, **kwargs)
        return wrapper
    return decorator


def home_page():
    # return render_template("/templates/index.html")
    return render_template("index.html")


def login_page():
    if request.method == "POST":
        username = request.form["email"]
        # print(username)
        # make sure that the user exists
        if current_app.config["mdb"].get_credentials_by_email(username) is not None:
            # pull information on this user
            user_credentials = current_app.config['mdb'].get_credentials_by_email(username)
            if user_credentials is not None:
                password = request.form["password"]
                # verify user's credentials
                if compare_password_hash(user_credentials['Password'], password):
                    # sets current user for use
                    user = get_user_object(user_credentials['UserID'])
                    login_user(user)
                    # if you were redirected to login first, go back used with @login_required
                    next_page = request.args.get("next", current_user.get_dashboard())
                    # print(user)
                    return redirect(next_page)
        else:
            flash("User not found")
        flash("Invalid credentials.")
    return render_template("login.html")


@login_required
def logout_page():
    logout_user()
    flash("You have logged out.")
    resp = make_response(render_template("login.html"))
    resp.set_cookie("Type", '', expires=0)
    return resp


@app.route("/sign-up", methods=["GET", "POST"])
def sign_up():
    if request.method == "POST":
        db = current_app.config["mdb"].db
        email = request.form["email"]
        if db.credentials.find({"Email": email}).count() > 0:
            flash("Email already used, input different email")
        else:
            current_app.config["mdb"].add_user(request.form)
            # return render_template("index.html")
            flash("Registration Succesful")
    return render_template("index.html")


@requiresUserType("admin")
def approvals():
    pat = patient()
    doc = doctor()
    co = communityOwner()
    ho = hospitalOwner()
    patients = pat.get_unapproved_patients()
    doctors = doc.get_unapproved_doctors()
    comm_owners = co.get_unapproved_communities()
    hosp_owners = ho.get_unapproved_hospitals()
    return render_template("approval.html", patients=patients, doctors=doctors, comm_owners=comm_owners,
                           hosp_owners=hosp_owners)


@requiresUserType("admin")
def approve_patient(UserID):
    pat = patient(UserID)
    pat.set_patient_approval()
    flash("Patient approved successfully")
    return approvals()


@requiresUserType("admin")
def approve_doctor(UserID):
    doc = doctor(UserID)
    doc.set_doctor_approval(UserID)
    flash("Doctor approved successfully")
    return approvals()


@requiresUserType("admin")
def approve_community_owner(CommunityID):
    co = communityOwner(CommunityID)
    co.set_community_approval()
    flash("Community Owner approved successfully")
    return approvals()


@requiresUserType("admin")
def approve_hospital_owner(HospitalID):
    ho = hospitalOwner(HospitalID)
    ho.set_hospital_approval()
    flash("Hospital Owner approved successfully")
    return approvals()


@requiresUserType("admin")
def delete_patient(UserID):
    pat = patient(UserID)
    pat.delete_patient()
    flash("Patient deleted successfully")
    return approvals()


@requiresUserType("admin")
def delete_doctor(UserID):
    doc = doctor(UserID)
    doc.delete_doctor()
    flash("Doctor deleted successfully")
    return approvals()


@requiresUserType("admin")
def delete_community_owner(UserID):
    co = communityOwner(UserID)
    co.delete_community_owner()
    flash("Community Owner deleted successfully")
    return approvals()


@requiresUserType("admin")
def delete_hospital_owner(UserID):
    ho = hospitalOwner(UserID)
    ho.delete_hospital_owner()
    flash("Hospital Owner deleted successfully")
    return approvals()


@requiresUserType("doctor")
def doctor_visit_page():
    if request.method == "POST":
        doctor.submitVisitForm(doctor, current_user.userID, request.form)
        return redirect(url_for("doctor_dashboard"))
    return render_template("doctor_visit.html")


@login_required
def get_home_dashboard():
    type = request.cookies.get('Type')
    if type == "Admin":
        return render_template("admin_dashboard.html")
    elif type == "Patient":
        return render_template("patient_dashboard.html")
    elif type == "Doctor":
        return render_template("doctor_dashboard.html")
    elif type == "HospitalOwner":
        return render_template("hospital_dashboard.html")
    elif type == "CommunityOwner":
        return render_template("community_dashboard.html")
    else:
        return render_template("index.html")


@requiresUserType("admin")
def admin_dashboard():
    resp = make_response(render_template("admin_dashboard.html"))
    resp.set_cookie("Type", 'Admin')
    return resp


@requiresUserType("doctor")
def doctor_dashboard():
    resp = make_response(render_template("doctor_dashboard.html"))
    resp.set_cookie("Type", "Doctor")
    return resp


# Grab a list of patients in a doctor's community
@requiresUserType("doctor")
def patient_record():
    db = current_app.config["mdb"]
    community = db.db.doctor.find_one({"UserID": current_user.userID})
    doc = doctor(community["CommunityID"])
    patients = doc.get_doctors_patients()
    return render_template("patient_record.html", patients=patients)


@requiresUserType("community_owner")
def community_owner_dashboard():
    resp = make_response(render_template("community_dashboard.html"))
    resp.set_cookie("Type", "CommunityOwner")
    return resp


@requiresUserType("hospital_owner")
def hospital_owner_dashboard():
    resp = make_response(render_template("hospital_dashboard.html"))
    resp.set_cookie("Type", "HospitalOwner")
    return resp


@requiresUserType("patient")
def patient_dashboard():
    resp = make_response(render_template("patient_dashboard.html"))
    resp.set_cookie("Type", "Patient")
    return resp


@requiresUserType("patient")
def input_biometrics():
    db = current_app.config["mdb"]
    bio_tests = db.get_biometric_tests()
    return render_template("input_biometrics.html", tests=bio_tests)


@requiresUserType("admin")
def add_biometric_tests():
    return render_template("insert_tests.html")


# Add a patient record of tests into database
@requiresUserType("patient")
def add_patient_record():
    db = current_app.config["mdb"]
    current_date = datetime.now()
    tests = db.get_biometric_tests()
    if request.method == "POST":
        for test in tests:
            test_id = test["TestID"]
            rec = record(current_user.userID, test_id, request.form[str(test_id)],
                         current_date.strftime("%d/%m/%Y %H:%M:%S"))
            rec.add_patient_record()
        flash("Patient Record inserted successfully")
    return input_biometrics()


def select_appointment():
    apmt = appointment(current_user.userID)
    appointments = apmt.find_available_appointments()
    return render_template("select_appointment.html", appointments=appointments, patient_id=current_user.userID)


# Reserve a appointment to a patient
@requiresUserType("patient")
def reserve_appointment(AppointmentID):
    apmt = appointment(AppointmentID, current_user.userID)
    apmt.assign_patient()
    flash("Appointment reserved successfully")
    return select_appointment()


# Adds the biometric test to the database
@requiresUserType("admin")
@app.route("/add-test", methods=["GET", "POST"])
def add_test():
    if request.method == "POST":
        bt = biometricTest(request.form["test-name"], request.form["lower-limit"], request.form["upper-limit"],
                           request.form["unit"])
        bt.add_biometric_test()
        flash("Biometric Test created successfully")
    return render_template("insert_tests.html")


# Get Trends and render to the page
@requiresUserType("patient")
def display_trends():
    db = current_app.config["mdb"]
    bio_tests = db.get_biometric_tests()
    table = []
    row = []
    for x in bio_tests:
        row.append(x.get('TestName'))
    table.append(row)
    for x in bio_tests:
        rec = record()
        rec.PatientID = current_user.userID
        rec.TestID = x.get('TestID')
        records = rec.get_records_by_date()
        lis = rec.get_sorted_patient_record_by_test()
        values = []
        for y in lis:
            values.append(y.get('TestValue'))
        table.append(values)

    return render_template("datatrends.html", tests=bio_tests, table=table, records=records)


@requiresUserType("doctor")
def display_patient_trends(PatientID):
    db = current_app.config["mdb"]
    bio_tests = db.get_biometric_tests()
    table = []
    row = []
    for x in bio_tests:
        row.append(x.get('TestName'))
    table.append(row)
    for x in bio_tests:
        rec = record()
        rec.PatientID = PatientID
        rec.TestID = x.get('TestID')
        records = rec.get_records_by_date()
        lis = rec.get_sorted_patient_record_by_test()
        values = []
        for y in lis:
            values.append(y.get('TestValue'))
        table.append(values)

    return render_template("doctor_datatrends.html", tests=bio_tests, table=table, records=records, patientid=PatientID)


# Alerts doctors about their patients' out-of-bounds biometrics
@requiresUserType("doctor")
def alert_patient():
    db = current_app.config["mdb"]
    community = db.db.doctor.find_one({"UserID": current_user.userID})
    doc = doctor(community["CommunityID"])
    patients = doc.get_doctors_patients()
    patients_ids = []
    for patient in patients:
        patients_ids.append(patient["UserID"])
    records = db.db.record.find({})
    alerts = []
    for record in records:
        if record["PatientID"] in patients_ids:
            tv = str(record["TestValue"])
            tv.replace("\"", "\'")
            test_value = float()
            lower_limit = biometricTest(record["TestID"]).get_test_lower_limit()
            upper_limit = biometricTest(record["TestID"]).get_test_upper_limit()
            if test_value < lower_limit["LowerLimit"]:
                alert = {}
                patient_name = db.get_name_from_userid(record["PatientID"])
                alert["UserID"] = record["PatientID"]
                alert["FirstName"] = patient_name["FirstName"]
                alert["LastName"] = patient_name["LastName"]
                test_name = biometricTest(record["TestID"]).get_test_name_from_id()["TestName"]
                condition = str(test_name) + " was too low on " + str(record["DateSubmitted"])
                alert["Condition"] = condition
                alert["Email"] = db.get_user_email(record["PatientID"])["Email"]
                alerts.append(alert)
            if test_value > upper_limit["UpperLimit"]:
                alert = {}
                patient_name = db.get_name_from_userid(record["PatientID"])
                alert["UserID"] = record["PatientID"]
                alert["FirstName"] = patient_name["FirstName"]
                alert["LastName"] = patient_name["LastName"]
                test_name = biometricTest(record["TestID"]).get_test_name_from_id()["TestName"]
                condition = str(test_name) + " was too high on " + str(record["DateSubmitted"])
                alert["Condition"] = condition
                alert["Email"] = db.get_user_email(record["PatientID"])["Email"]
                alerts.append(alert)
    return render_template("alert_patient.html", alerts=alerts)


# Adds appointment to the database
@app.route("/set_appointments", methods=["GET", "POST"])
@requiresUserType("doctor")
def set_appointments():
    if request.method == "POST":
        apmt = appointment(current_user.userID, request.form["appointment-date"], request.form["appointment-time"])
        apmt.add_appointment()
        flash("Appointment entered successfully")
    return render_template("set_appointments.html")


# Render table page of reserved appointments
@requiresUserType("doctor")
def reserved_appointments():
    apmt = appointment(current_user.userID)
    appointments = apmt.find_reserved_appointments()
    return render_template("reserved_appointments.html", appointments=appointments)


# Delete appointment from reserved appointments table
@requiresUserType("doctor")
def delete_appointment(AppointmentID):
    apmt = appointment(AppointmentID)
    apmt.delete_appointment()
    flash("Appointment deleted successfully")
    return reserved_appointments()


@app.route("/matplot-as-image-<int:testid>.png")
#TODO does '/matplot-as-image...' need to restrict user types
def plot_png(testid):
    rec = record()
    rec.PatientID = current_user.userID
    rec.TestID = testid
    lis = rec.get_sorted_patient_record_by_test()
    values = []
    dates = []
    for x in lis:
        if not x.get('TestValue') == '':
            dates.append(x.get('DateSubmitted'))
            values.append(x.get('TestValue'))
    f_Values = [float(line) for line in values]
    fig = Figure()
    axis = fig.add_subplot(1, 1, 1)
    axis.grid('True')
    axis.plot(dates, f_Values, color='blue', linewidth=3)
    output = io.BytesIO()
    FigureCanvasAgg(fig).print_png(output)
    return Response(output.getvalue(), mimetype="image/png")


@app.route("/doctor/dashboard/patient_record/matplot-as-image-<int:testid>.png")
@requiresUserType("doctor")
def plot_png_for_doctor(testid, patientid):
    rec = record()
    rec.PatientID = patientid
    rec.TestID = testid
    lis = rec.get_sorted_patient_record_by_test()
    values = []
    dates = []
    for x in lis:
        if not x.get('TestValue') == '':
            dates.append(x.get('DateSubmitted'))
            values.append(x.get('TestValue'))
    f_Values = [float(line) for line in values]
    fig = Figure()
    axis = fig.add_subplot(1, 1, 1)
    axis.grid('True')
    axis.plot(dates, f_Values, color='blue', linewidth=3)
    output = io.BytesIO()
    FigureCanvasAgg(fig).print_png(output)
    return Response(output.getvalue(), mimetype="image/png")


@requiresUserType("doctor")
def doctor_patient_page(DoctorID, PatientID):
    db = current_app.config["mdb"]
    patient = get_user_object(PatientID)
    doctor = get_user_object(DoctorID)
    visits = db.get_patient_visits(DoctorID, PatientID)
    return render_template("doctor_patient_visits.html", visits=visits, patient=patient, doctor=doctor)


@requiresUserType("doctor")
def doctor_visit_details(VisitID):
    db = current_app.config["mdb"]
    visit = db.get_visit(VisitID)
    if visit is not None:
        patient = get_user_object(visit['Patient'])
        doctor = get_user_object(visit['DoctorID'])
        prescription = db.get_prescription(VisitID)
        return render_template("visit_information.html", visit=visit, prescriptions=prescription,
                               patient=patient, doctor=doctor)


@requiresUserType("doctor")
def patient_visits():
    db = current_app.config["mdb"]
    patients = db.get_matching_patients(current_user.userID)
    patient_info = {}
    for patient in patients:
        patient_info[patient] = get_user_object(patient)
    patients = []
    for patient in patient_info.keys():
        patients.append(patient_info[patient])
    doctor = get_user_object(current_user.userID)
    return render_template("doctor_search_patient_visits.html", patients=patients, doctor=doctor)


@requiresUserType("admin")
def get_report():
    db = current_app.config["mdb"]
    hospitals, communities = db.get_hospitals_and_communities()
    report = []
    selections = {'show_me': str(None), 'specification_type': str(None), 'specification_value': str(None), 'want_exceeded': str(None)}
    if request.method == "POST":
        if request.form['specification_type'] == 'None':
            specification_type = None
        else:
            specification_type = request.form['specification_type']
        if 'specification_value' in request.form.keys():
            specification_value = request.form['specification_value']
        else:
            specification_value = None
        if 'want_exceeded' in request.form.keys():
            want_exceeded = request.form['want_exceeded']
        else:
            want_exceeded = None
        selections = {'show_me': str(request.form['show_me']), 'specification_type': str(specification_type), 'specification_value': str(specification_value), 'want_exceeded': str(want_exceeded)}
        try:
            int(specification_value)
            selections[specification_value] = int(specification_value)
        except:
            pass
        report = admin.admin().generate_reports(request.form['show_me'], specification_type, specification_value, want_exceeded)
    if len(report) > 0:
        keys = report[0].keys()
    else:
        keys = []
    return render_template("admin_reports.html", report=report, report_keys=keys, possible_show_me=['Patients', 'Doctors', 'Hospitals', 'Communities'], specification_types=['None','Hospital', 'Community'], hospitals=hospitals, communities=communities, selections=selections)


@requiresUserType("community_owner")
def community_owner_get_report():
    db = current_app.config["mdb"]
    this_community = db.db['community'].find_one({"CommunityOwnerID": current_user.userID}, {'_id': 0})
    hospitals, communities = db.get_hospitals_and_communities(this_community['CommunityID'])
    report = []
    selections = {'show_me': str(None), 'specification_type': str(None), 'specification_value': str(None), 'want_exceeded': str(None)}
    if request.method == "POST":
        if request.form['specification_type'] == 'None':
            specification_type = None
        else:
            specification_type = request.form['specification_type']
        if 'specification_value' in request.form.keys():
            specification_value = request.form['specification_value']
        else:
            specification_value = None
        if 'want_exceeded' in request.form.keys():
            want_exceeded = request.form['want_exceeded']
        else:
            want_exceeded = None
        selections = {'show_me': str(request.form['show_me']), 'specification_type': str(specification_type), 'specification_value': str(specification_value), 'want_exceeded': str(want_exceeded)}
        try:
            int(specification_value)
            selections[specification_value] = int(specification_value)
        except:
            pass
        report = communityOwner(current_user.userID).generate_reports(request.form['show_me'], specification_type, specification_value, want_exceeded)

    if len(report) > 0:
        keys = report[0].keys()
    else:
        keys = []
    return render_template("community_owner_reports.html", report=report, report_keys=keys, possible_show_me=['Patients', 'Doctors', 'Hospitals'], specification_types=['None','Hospital'], hospitals=hospitals, communities=communities, selections=selections)
