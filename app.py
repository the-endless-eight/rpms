from flask import Flask, request, redirect, url_for, flash
import os.path

import admin
import views
from rpms_database import Database
from flask_pymongo import PyMongo
from flask_login import LoginManager
from user import get_user_object

lm = LoginManager()


@lm.user_loader
def load_user(user_id):
    return get_user_object(user_id)


def create_app():
    app = Flask(__name__)
    app.SECRET_KEY = ""

    app.add_url_rule("/", view_func=views.home_page)
    app.add_url_rule("/home", view_func=views.home_page)
    app.add_url_rule("/login", view_func=views.login_page, methods=["GET", "POST"])
    app.add_url_rule("/logout", view_func=views.logout_page)
    app.add_url_rule("/sign-up", view_func=views.sign_up, methods=["GET", "POST"])
    app.add_url_rule("/approvals", view_func=views.approvals, methods=["GET", "POST"])
    app.add_url_rule("/approvals/approve_patient/<int:UserID>", view_func=views.approve_patient)
    app.add_url_rule("/approvals/approve_doctor/<int:UserID>", view_func=views.approve_doctor)
    app.add_url_rule("/approvals/approve_community_owner/<int:CommunityID>", view_func=views.approve_community_owner)
    app.add_url_rule("/approvals/approve_hospital_owner/<int:HospitalID>", view_func=views.approve_hospital_owner)
    app.add_url_rule("/approvals/delete_patient/<int:UserID>", view_func=views.delete_patient)
    app.add_url_rule("/approvals/delete_doctor/<int:UserID>", view_func=views.delete_doctor)
    app.add_url_rule("/approvals/delete_community_owners/<int:UserID>", view_func=views.delete_community_owner)
    app.add_url_rule("/approvals/delete_hospital_owners/<int:UserID>", view_func=views.delete_hospital_owner)
    app.add_url_rule("/doctor/visit", view_func=views.doctor_visit_page, methods=["GET", "POST"])
    app.add_url_rule("/dashboard", view_func=views.get_home_dashboard)
    app.add_url_rule("/doctor/dashboard", view_func=views.doctor_dashboard)
    app.add_url_rule("/patient/dashboard", view_func=views.patient_dashboard)
    app.add_url_rule("/hospital/dashboard", view_func=views.hospital_owner_dashboard)
    app.add_url_rule("/community/dashboard", view_func=views.community_owner_dashboard)
    app.add_url_rule("/admin/dashboard", view_func=views.admin_dashboard)
    
    app.add_url_rule("/patient/dashboard/input_biometrics", view_func=views.input_biometrics)
    app.add_url_rule("/patient/dashboard/input_biometrics/add_patient_record", view_func=views.add_patient_record, methods=["GET", "POST"])
    app.add_url_rule("/admin/dashboard/add_biometric_tests", view_func=views.add_biometric_tests)
    app.add_url_rule("/add-test", view_func=views.add_test, methods=["GET", "POST"])
    app.add_url_rule("/patient/dashboard/data_trends", view_func=views.display_trends, methods=["GET", "POST"])
    app.add_url_rule("/patient/dashboard/select_appointment", view_func=views.select_appointment)
    app.add_url_rule("/patient/dashboard/select_appointment/reserve_appointment/<int:AppointmentID>", view_func=views.reserve_appointment)
    app.add_url_rule("/matplot-as-image-<int:testid>.png", view_func=views.plot_png)

    app.add_url_rule("/visits/<int:DoctorID>/<int:PatientID>", view_func=views.doctor_patient_page)
    app.add_url_rule("/view-visit/<int:VisitID>", view_func=views.doctor_visit_details)
    app.add_url_rule("/doctor/dashboard/patient_visits", view_func=views.patient_visits)
    app.add_url_rule("/doctor/dashboard/patient_record", view_func=views.patient_record)
    app.add_url_rule("/doctor/dashboard/patient_record/<int:PatientID>", view_func=views.display_patient_trends, methods=["GET", "POST"])
    app.add_url_rule("/doctor/dashboard/patient_record/<int:patientid>/matplot-as-image-<int:testid>.png", view_func=views.plot_png_for_doctor)
    app.add_url_rule("/doctor/dashboard/alert_patient", view_func=views.alert_patient)
    app.add_url_rule("/doctor/dashboard/set_appointments", view_func=views.set_appointments, methods=["GET", "POST"])
    app.add_url_rule("/doctor/dashboard/reserved_appointments", view_func=views.reserved_appointments)
    app.add_url_rule("/patient/dashboard/reserved_appointments/delete_appointment/<int:AppointmentID>",
                     view_func=views.delete_appointment)

    app.add_url_rule("/admin/dashboard/reports", view_func=views.get_report, methods=["GET", "POST"])
    app.add_url_rule("/community/dashboard/reports", view_func=views.community_owner_get_report, methods=["GET", "POST"])

    # configuration handling
    app.config.from_object("settings")

    # database
    mongo = PyMongo(app)
    mdb = Database(mongo.db)
    app.config["mdb"] = mdb

    # login things
    lm.init_app(app)
    lm.login_view = "login_page"
    app.config['enforces_user_type'] = True

    return app


if __name__ == "__main__":
    app = create_app()
    app.run(debug=True)
    port = app.config.get("PORT", 8080)
    app.run(host="0.0.0.0", port=port)
