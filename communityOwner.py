from flask import current_app, flash
import random


# Generates an integer of length 12 (skips 0 to 99999999999 for simplicity in code)
def generate_random_id():
    return random.randint(100000000000, 999999999999)


class communityOwner:
    def __init__(self, *args):
        self.db = current_app.config["mdb"].db
        if len(args) > 1:
            self.CommunityName = args[0]
            self.Address = args[1]
            self.CommunityOwnerID = args[2]
            self.Approved = False
        elif len(args) == 1:
            self.CommunityID = args[0]
            self.CommunityOwnerID = args[0]

    # Returns all communities where they're not approved.
    def get_unapproved_communities(self):
        db = current_app.config["mdb"]
        communities = db.list_communities()
        for community in communities:
            if "CommunityOwnerID" in community:
                community["Owner"] = db.get_name_from_userid(community["CommunityOwnerID"])
        return communities

    def add_community_owner(self):
        comid = generate_random_id()
        self.db.community.insert(
            {
                "CommunityID": comid,
                "CommunityName": self.CommunityName,
                "Address": self.Address,
                "CommunityOwnerID": self.CommunityOwnerID,
                "Approved": self.Approved
            }
        )

    # WILL BREAK IF COMMUNITY IS OWNED AND USED. ONLY DELETES MAIN RECORDS OF IT EXISTING
    def delete_community_owner(self):
        self.db.community.delete_one({"CommunityOwnerID": self.CommunityOwnerID})
        self.db.user.delete_one({"UserID": self.CommunityOwnerID})
        self.db.credentials.delete_one({"UserID": self.CommunityOwnerID})

    # Doesn't take into account if the person approving is an admin or not.
    def set_community_approval(self):
        return self.db.community.update({"CommunityID": self.CommunityID}, {"$set": {"Approved": True}})

    def generate_reports(self, show_me, specification_type=None, specification_value=None, want_exceeded=False):

        # show_me = type of query
        # specification_type = if it's a hospital/community
        # specification_value = the id of the hospital/community

        this_community = self.db['community'].find_one({"CommunityOwnerID": self.CommunityOwnerID}, {'_id': 0})

        if this_community != None:

            if show_me in ['Patients', 'Doctors', 'Hospitals', 'Communities']:

                query_constraint = {}
                database = None
                if show_me == 'Patients' or show_me == 'Doctors' or show_me == 'Hospitals':
                    database = show_me.lower()[:-1]
                elif show_me == 'Communities':
                    database = 'community'

                if specification_type is not None and specification_value is not None:
                    if specification_type == 'Hospital':
                        query_constraint |= {'HospitalID': specification_value}
                query_constraint |= {'CommunityID': str(this_community['CommunityID'])}

                if database == 'patient':
                    if 'HospitalID' in query_constraint.keys():
                        query_constraint.pop('HospitalID')

                if database == 'community':
                    if 'CommunityID' in query_constraint.keys():
                        query_constraint['CommunityID'] = int(query_constraint['CommunityID'])
                if database == 'hospital':
                    if 'HospitalID' in query_constraint.keys():
                        query_constraint['HospitalID'] = int(query_constraint['HospitalID'])

                if database is not None:

                    values = list(self.db[database].find(query_constraint, {'_id': 0}))
                    if len(values) > 0:
                        report_data = []
                        if show_me == 'Patients' or show_me == 'Doctors':
                            for person in values:
                                this_person = {'UserID': person['UserID']}
                                user = list(self.db['user'].find({'UserID': person['UserID']}, {'_id': 0}))
                                if len(user) >= 1:
                                    user = user[0]
                                    this_person['First Name'] = user['FirstName']
                                    this_person['Last Name'] = user['LastName']
                                    report_data.append(this_person)

                            if want_exceeded:
                                exceeding = set()
                                for user in report_data:
                                    records = list(self.db.record.find({"PatientID": user['UserID']}, {'_id': 0}))
                                    if len(records) > 0:
                                        test_ids = []
                                        for record in records:
                                            test_ids.append(record["TestID"])

                                        tests = list(
                                            self.db.biometric_test.find({"TestID": {"$in": test_ids}}, {'_id': 0}))

                                        test_data = {}
                                        for test in tests:
                                            test_data[test["TestID"]] = test

                                        for record in records:
                                            if record["TestID"] in test_data.keys():
                                                try:
                                                    value = float(record["TestValue"])
                                                    if value > test_data[record["TestID"]]["UpperLimit"] or value < \
                                                            test_data[record["TestID"]]["LowerLimit"]:
                                                        exceeding.add(record['PatientID'])
                                                except:
                                                    pass

                                for user in report_data.copy():
                                    if user['UserID'] not in exceeding:
                                        report_data.remove(user)
                            return report_data

                        elif show_me == 'Hospitals':
                            hospitals = values
                            report_data = []
                            for hospital in hospitals:
                                hospital_data = {'HospitalID': hospital['HospitalID'], 'Name': hospital['HospitalName'], 'Address': hospital['Address'], 'CommunityID': hospital['CommunityID']}
                                report_data.append(hospital_data)
                            return report_data

            return []
        else:
            return [{'error': 'Unknown owner for a community...'}]