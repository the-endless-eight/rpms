from flask import current_app, render_template, request


def home_page():
    db = current_app.config["mdb"]
    data = db.get_students()
    return render_template("home.html", data=data)


def add_student():
    return edit_student()


def edit_student(studentID=-1):
    if request.method == "POST":
        db = current_app.config["mdb"]
        db.edit_student(request.form)
        return home_page()

    db = current_app.config["mdb"]
    student = db.get_student(studentID)
    if not student.count() == 0:
        student = student[0]

    return render_template("edit.html", student=student)


def delete_student(studentID):
    db = current_app.config["mdb"]
    db.delete_student(studentID)
    return home_page()
