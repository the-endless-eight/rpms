from flask import Flask
import views
from Examples.mdatabase import mDatabase
from flask_pymongo import PyMongo


def create_app():
    out = Flask(__name__)

    out.add_url_rule("/", view_func=views.home_page)
    out.add_url_rule("/add_student", view_func=views.add_student, methods=["GET", "POST"])
    out.add_url_rule("/edit_student/<int:studentID>", view_func=views.edit_student, methods=["GET", "POST"])
    out.add_url_rule("/delete/<int:studentID>", view_func=views.delete_student)

    out.config.from_object("settings")

    mongo = PyMongo(out)
    mdb = mDatabase(mongo.db)
    out.config["mdb"] = mdb
    return out


if __name__ == "__main__":
    app = create_app()
    port = app.config.get("PORT", 8080)
    app.run(host="0.0.0.0", port=port)
