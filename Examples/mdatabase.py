from flask import flash


class mDatabase:
    def __init__(self, db):
        self.db = db

    def get_students(self):
        return self.db.Students.find({})

    def get_student(self, studentID):
        return self.db.Students.find({"ID": studentID})

    def edit_student(self, form):
        previousRecord = self.db.Students.find({
            "ID": int(form['ID'])
        })
        print(previousRecord.count())
        if previousRecord.count():
            print(form["Name"])
            self.db.Students.update_one(
                {"ID": int(form['ID'])},
                {"$set": {
                    "ID": int(form["ID"]),
                    "Name": form["Name"],
                    "Email": form["Email"],
                    "Major": form["Major"]
                }})
            flash("record updated successfully")
        else:
            self.db.Students.insert(
                {
                    "ID": int(form["ID"]),
                    "Name": form["Name"],
                    "Email": form["Email"],
                    "Major": form["Major"]
                })
            flash("record created successfully")

    def delete_student(self, ID):
        self.db.Students.delete_one({"ID": ID})
        flash("record removed successfully")
