from numbers import Number

from flask import current_app, flash
import random

# Generates an integer of length 12 (skips 0 to 99999999999 for simplicity in code)
def generate_random_id():
    return random.randint(100000000000, 999999999999)

class biometricTest:
    def __init__(self, *args):
        self.db = current_app.config["mdb"].db
        if len(args) > 1:
            self.TestName = args[0]
            self.LowerLimit = args[1]
            self.UpperLimit = args[2]
            self.Unit = args [3]
        elif len(args) == 1:
            self.TestID = args[0]
            self.TestName = args[0]

    # Add biometric test
    def add_biometric_test(self):
        self.db.biometric_test.insert(
            {
                "TestID": generate_random_id(),
                "TestName": self.TestName,
                "LowerLimit": float(self.LowerLimit),
                "UpperLimit": float(self.UpperLimit),
                "Unit": self.Unit,
            }
        )

    # Grab test based on test id
    def get_test_from_id(self):
        return self.db.biometric_test.find_one({"TestID": self.TestID})

    # Grab test based on test name
    def get_test_from_name(self):
        return self.db.biometric_test.find_one({"TestName": self.TestName})

    # Grab test id based on test name
    def get_test_id_from_name(self):
        return self.db.biometric_test.find_one({"TestName": self.TestName}, {"TestID": 1})

    # Grab test name based on test id
    def get_test_name_from_id(self):
        return self.db.biometric_test.find_one({"TestID": self.TestID}, {"TestName": 1})

    # Grab lower limit of test based on test id
    def get_test_lower_limit(self):
        return self.db.biometric_test.find_one({"TestID": self.TestID}, {"LowerLimit": 1})

    # Grab upper limit of test based on test id
    def get_test_upper_limit(self):
        return self.db.biometric_test.find_one({"TestID": self.TestID}, {"UpperLimit": 1})



