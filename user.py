from flask import current_app, url_for
from flask_login import UserMixin


# having a user object allows us to login, and track users.
# We can also store frequently referenced user dependent information in this object
class User(UserMixin):
    def __init__(self, user_id):
        # user things
        db = current_app.config["mdb"]
        self.user = db.get_user(user_id)
        #print(self.user)
        self.userID = self.user["UserID"]
        self.fName = self.user["FirstName"]
        self.lName = self.user["LastName"]

        # user type flags and things

        self.is_logged_in = True
        self.is_patient = False
        self.is_doctor = False
        self.is_community_owner = False
        self.is_hospital_owner = False
        self.is_admin = False

        # contains any records if user is one of the above 3
        self.patient_record = None
        self.doctor_record = None
        self.owned_community_record = None
        self.owned_hospital_record = None

        self.active = True

    def get_id(self):
        return self.userID

    @property
    def is_active(self):
        return self.active

    def get_dashboard(self):
        print(self.is_admin)
        if self.is_admin:
            return url_for("admin_dashboard")
        if self.is_doctor:
            return url_for("doctor_dashboard")
        if self.is_community_owner:
            return url_for("community_owner_dashboard")
        if self.is_hospital_owner:
            return url_for("hospital_owner_dashboard")
        # print("end self")
        if self.is_patient:
            return url_for("patient_dashboard")
        return url_for("home_page")


def get_user_object(user_id):
    user = User(user_id)

    if user is None:
        return None
    populate_user_info(user)

    # some useful flags for displaying info to certain users and not others
    #print("patient_record:" + str(user.patient_record))
    if user.patient_record.count() != 0:
        #print("logging in as patient")
        user.is_patient = True
    if user.doctor_record.count() != 0:
        #print("logging in as doctor")
        user.is_doctor = True
    if user.owned_community_record.count() != 0:
        #print("logging in as community owner")
        user.is_community_owner = True
    if user.owned_hospital_record.count() != 0:
        #print("logging in as hospital owner")

        user.is_hospital_owner = True

    try:
        if user.user['IsAdmin']:
            user.is_admin = True
            #print("admin logs in")
    except KeyError as e:
        #print("user logs in as not admin error:" + str(e))
        pass

    return user


def populate_user_info(user):
    uid = user.userID
    db = current_app.config['mdb'].db
    user.patient_record = db.patient.find({"UserID": uid})
    user.doctor_record = db.doctor.find({"UserID": uid})
    user.owned_community_record = db.community.find({"CommunityOwnerID": uid})
    user.owned_hospital_record = db.hospital.find({"HospitalOwnerID": uid})
    return user
