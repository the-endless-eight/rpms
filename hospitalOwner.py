from flask import current_app, flash
import random

# Generates an integer of length 12 (skips 0 to 99999999999 for simplicity in code)
def generate_random_id():
    return random.randint(100000000000, 999999999999)


class hospitalOwner:
    # Constructors for inserting hospital/hospital owner or with only UserID and HospitalID
    def __init__(self, *args):
        self.db = current_app.config["mdb"].db
        if len(args) > 1:
            self.UserID = args[0]
            self.HospitalName = args[1]
            self.CommunityID = args[2]
            self.HospAddress = args[3]
            self.Approved = False
        elif len(args) == 1:
            # Constructors for updating and deleting hospital/hospital owner
            self.UserID = args[0]
            self.HospitalID = args[0]

    # Returns all hospitals where they're not approved.
    def get_unapproved_hospitals(self):
        db = current_app.config["mdb"]
        hospitals = db.list_hospital()
        for hospital in hospitals:
            if "HospitalOwnerID" in hospital:
                hospital["Owner"] = db.get_name_from_userid(
                    hospital["HospitalOwnerID"]
                )
        return hospitals

    # Insert hospital owner into database
    def add_hospital_owner(self):
        hosid = generate_random_id()
        self.db.hospital.insert(
            {
                "HospitalID": hosid,
                "HospitalName": self.HospitalName,  # NEEDS TO BE CHANGED WHEN IT'S FIGURED OUT
                "CommunityID": self.CommunityID,  # NEEDS TO BE CHANGED WHEN IT'S FIGURED OUT
                "Address": self.HospAddress,  # NEEDS TO BE CHANGED WHEN IT'S FIGURED OUT
                "HospitalOwnerID": self.UserID,
                "Approved": self.Approved,  # assuming we need to approve that hospitals aren't fake/spam
            }
        )

    # WILL BREAK IF HOSPITAL IS OWNED AND USED. ONLY DELETES MAIN RECORDS OF IT EXISTING
    def delete_hospital_owner(self):
        self.db.hospital.delete_one({"HospitalOwnerID": self.UserID})
        self.db.user.delete_one({"UserID": self.UserID})
        self.db.credentials.delete_one({"UserID": self.UserID})

    # Doesn't take into account if the person approving is an community owner or not.
    def set_hospital_approval(self):
        return self.db.hospital.update(
            {"HospitalID": self.HospitalID}, {"$set": {"Approved": True}}
        )
