from flask import current_app, flash
import random

# Generates an integer of length 12 (skips 0 to 99999999999 for simplicity in code)
def generate_random_id():
    return random.randint(100000000000, 999999999999)

class appointment:
    # constructor
    def __init__(self, *args):
        self.db = current_app.config["mdb"].db
        if len(args) == 3:
            self.UserID = args[0]
            self.Date = args[1]
            self.Time = args[2]
        elif len(args) == 2:
            self.AppointmentID = args[0]
            self.UserID = args[1]
        elif len(args) == 1:
            self.AppointmentID = args[0]
            self.UserID = args[0]

    # Adds appointment to database
    def add_appointment(self):
        self.db.appointment.insert(
            {
                "AppointmentID": generate_random_id(),
                "DoctorID": self.UserID,
                "Date": self.Date,
                "Time": self.Time,
                "Reserved": False
            }
        )

    # Delete appointment from database
    def delete_appointment(self):
        self.db.appointment.delete_one({"AppointmentID": self.AppointmentID})

    # Assigns an appointment to a patient
    def assign_patient(self):
        self.db.appointment.update({"AppointmentID": self.AppointmentID}, {"$set": {"PatientID": self.UserID, "Reserved": True}})

    # Finds available appointments of doctors in the same community as patient
    def find_available_appointments(self):
        community_id = self.db.patient.find_one({"UserID": self.UserID}, {"CommunityID": 1})
        appointments = self.db.appointment.find({"Reserved": False}).sort([("Date", 1), ("Time", 1)])
        appointment_list = []
        for appointment in appointments:
            if self.db.doctor.count({"UserID": appointment["DoctorID"], "CommunityID": community_id["CommunityID"]}) > 0:
                apmt = {}
                apmt["AppointmentID"] = appointment["AppointmentID"]
                docs_name = self.db.user.find_one({"UserID": appointment["DoctorID"]}, {"FirstName": 1, "LastName": 1})
                name = docs_name["FirstName"] + " " + docs_name["LastName"]
                apmt["DoctorName"] = name
                apmt["Date"] = appointment["Date"]
                apmt["Time"] = appointment["Time"]
                appointment_list.append(apmt)
        return appointment_list

    # Finds reserved appointments for doctors
    def find_reserved_appointments(self):
        appointments = self.db.appointment.find({"Reserved": True}).sort([("Date", 1), ("Time", 1)])
        appointment_list = []
        for appointment in appointments:
            if appointment["DoctorID"] == self.UserID:
                apmt = {}
                apmt["AppointmentID"] = appointment["AppointmentID"]
                apmt["PatientID"] = appointment["PatientID"]
                patient = self.db.user.find_one({"UserID": appointment["PatientID"]}, {"FirstName": 1, "LastName": 1})
                name = patient["FirstName"] + " " + patient["LastName"]
                apmt["PatientName"] = name
                apmt["Date"] = appointment["Date"]
                apmt["Time"] = appointment["Time"]
                appointment_list.append(apmt)
        return appointment_list

