import copy
import json

from flask import current_app

from rpms_database import generate_random_id, hash_password


class admin:
    # Constructor for inserting admin user
    def __init__(self, *args):
        self.db = current_app.config["mdb"]
        if len(args) > 1:
            self.UserID = args[0]
            self.FirstName = args[1]
            self.LastName = args[2]
            self.BirthDate = args[3]
            self.Address = args[4]
            self.Address2 = args[5]
            self.Email = args[6]
            self.Password = args[7]
            self.SSN = args[8]
            self.IsAdmin = True
        elif len(args) == 1:
            # Constructor for updating or deleting admin
            self.UserID = args[0]

    def add_admin(self):
        user_id = generate_random_id()
        self.db.user.insert(
            {
                "UserID": user_id,
                "FirstName": self.FirstName,
                "LastName": self.LastName,
                "Address": self.Address,
                "Address-2": self.Address2,
                "Birthday": self.BirthDate,
                "IsAdmin": self.IsAdmin,
            }
        )
        self.db.credentials.insert(
            {
                "UserID": user_id,
                "SSN": hash_password(self.SSN),
                "Email": self.Email,
                "Password": hash_password(self.Password)
            }
        )

    def generate_reports(self, show_me, specification_type=None, specification_value=None, want_exceeded=False):

        # show_me = type of query
        # specification_type = if it's a hospital/community
        # specification_value = the id of the hospital/community

        if show_me in ['Patients', 'Doctors', 'Hospitals', 'Communities']:

            query_constraint = {}
            database = None
            if show_me == 'Patients' or show_me == 'Doctors' or show_me == 'Hospitals':
                database = show_me.lower()[:-1]
            elif show_me == 'Communities':
                database = 'community'

            if specification_type is not None and specification_value is not None:
                if specification_type == 'Hospital':
                    query_constraint |= {'HospitalID': specification_value}
                elif specification_type == 'Community':
                    query_constraint |= {'CommunityID': specification_value}

            if database == 'patient':
                if 'HospitalID' in query_constraint.keys():
                    query_constraint.pop('HospitalID')

            if database == 'community':
                if 'CommunityID' in query_constraint.keys():
                    query_constraint['CommunityID'] = int(query_constraint['CommunityID'])
            if database == 'hospital':
                if 'HospitalID' in query_constraint.keys():
                    query_constraint['HospitalID'] = int(query_constraint['HospitalID'])

            if database is not None:

                values = list(self.db.db[database].find(query_constraint, {'_id': 0}))
                if len(values) > 0:
                    report_data = []
                    if show_me == 'Patients' or show_me == 'Doctors':
                        for person in values:
                            this_person = {'UserID': person['UserID']}
                            user = list(self.db.db['user'].find({'UserID': person['UserID']}, {'_id': 0}))
                            if len(user) >= 1:
                                user = user[0]
                                this_person['First Name'] = user['FirstName']
                                this_person['Last Name'] = user['LastName']
                                report_data.append(this_person)

                        if want_exceeded:
                            exceeding = set()
                            for user in report_data:
                                records = list(self.db.db.record.find({"PatientID": user['UserID']}, {'_id': 0}))
                                if len(records) > 0:
                                    test_ids = []
                                    for record in records:
                                        test_ids.append(record["TestID"])

                                    tests = list(
                                        self.db.db.biometric_test.find({"TestID": {"$in": test_ids}}, {'_id': 0}))

                                    test_data = {}
                                    for test in tests:
                                        test_data[test["TestID"]] = test

                                    for record in records:
                                        if record["TestID"] in test_data.keys():
                                            try:
                                                value = float(record["TestValue"])
                                                if value > test_data[record["TestID"]]["UpperLimit"] or value < \
                                                        test_data[record["TestID"]]["LowerLimit"]:
                                                    exceeding.add(record['PatientID'])
                                            except:
                                                pass

                            for user in report_data.copy():
                                if user['UserID'] not in exceeding:
                                    report_data.remove(user)
                        return report_data

                    elif show_me == 'Hospitals':
                        hospitals = values
                        report_data = []
                        for hospital in hospitals:
                            hospital_data = {'HospitalID': hospital['HospitalID'], 'Name': hospital['HospitalName'], 'Address': hospital['Address'], 'CommunityID': hospital['CommunityID']}
                            report_data.append(hospital_data)
                        return report_data

                    elif show_me == 'Communities':
                        communities = values
                        report_data = []
                        for community in communities:
                            community_data = {'CommunityID': community['CommunityID'], 'Name': community['CommunityName'], 'Address': community['Address']}
                            report_data.append(community_data)
                        return report_data
        return []
